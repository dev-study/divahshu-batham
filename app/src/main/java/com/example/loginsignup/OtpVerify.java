package com.example.loginsignup;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class OtpVerify extends AppCompatActivity {
    EditText otp_textbox_one,otp_textbox_two,otp_textbox_three,otp_textbox_four;
    Button otpVerify;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp_verify);

        otp_textbox_one = (EditText)findViewById(R.id.otp_edit_box1);
        otp_textbox_two = (EditText)findViewById(R.id.otp_edit_box2);
        otp_textbox_three = (EditText)findViewById(R.id.otp_edit_box3);
        otp_textbox_four = (EditText)findViewById(R.id.otp_edit_box4);

        otpVerify = (Button)findViewById(R.id.otpverify);

        EditText[] edit = {otp_textbox_one, otp_textbox_two, otp_textbox_three, otp_textbox_four};

        otp_textbox_one.addTextChangedListener(new GenericTextWatcher(otp_textbox_one, edit));
        otp_textbox_two.addTextChangedListener(new GenericTextWatcher(otp_textbox_two, edit));
        otp_textbox_three.addTextChangedListener(new GenericTextWatcher(otp_textbox_three, edit));
        otp_textbox_four.addTextChangedListener(new GenericTextWatcher(otp_textbox_four, edit));

        otpVerify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(OtpVerify.this,Sign_up.class);
                Toast.makeText(getApplicationContext(),"Login Successfully",Toast.LENGTH_SHORT).show();
                startActivity(intent);
            }
        });


    }
}